# EDTS TDP - Android Development Labs #

List of branch:

### Master ###

* Initial Project

### Basic Component ###

* Activity
* Fragment
* Intent

### Layout ###

* View & View Group
* Listview & Recyclerview
* Style & Theme

### Background Process & Networking ###

* Backgdround thread
* Web API & Json
* Retrofit

### Local Data Persistent ###

* Shared Preferences
* Room

### Architecture Component ###
* Modern Android Development
* Android Jetpack
* ViewModel
* LiveData
* Repository"